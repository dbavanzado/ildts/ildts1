//
//  SQLif.swift
//  iLDTS-3
//
//  Created by Josemaria Carazo Abolafia on 17/09/2019.
//  Copyright © 2019 Josemaria Carazo Abolafia. All rights reserved.
//

import Foundation

class SQLif {
    // Get the URL to db store file
    let db : dataconn
    var isopen : Bool
    // Code to delete a db file. Useful to invoke in case of a corrupt DB and re-create another
    
    init() {
        db = dataconn()
        isopen = false
        open()
    }
    
    func open() {
        if (!isopen) {
            do {
                try db.openDB()
                isopen = true
            } catch {
                print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
                isopen = false
            }
        }
    }
    
    func close() {
        if (isopen) {
            db.closeDB()
            isopen = false
        }
    }
    
    func canbeopen() -> Bool {
        if (!isopen) { open() }
        return isopen
    }
    
    func query2tabla(queryStatementString: String) -> [[String]] {
        if (canbeopen()) {
            return db.queryA26(queryStatementString: queryStatementString as NSString)
        } else {
            return [[""]]
        }
    }
    
    func datosql(query: String) -> String {
        var resultado = [[""]]
        if (canbeopen()) {
            resultado = db.queryA26(queryStatementString: query as NSString)
        }
        return resultado[0][0]
    }

    func existeregistro(query: String) -> Bool {
        var resultado = false
        if (canbeopen()) {
            let resultados = db.queryA26(queryStatementString: query as NSString)
            if (resultados != [["",""]]) {
                resultado = true
            }
        }
        return resultado
    }

}
