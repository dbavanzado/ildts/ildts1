//
//  ContentView.swift
//  ildts1
//
//  Created by Josemaria Carazo Abolafia on 11/5/21.
//

import SwiftUI

struct ContentView: View {

    //Es esencial que sea @State para que los usos que se hacen de la variable se actualicen cuando su valor cambie.
    @State var queryresult :String = ""
    
    init() {
    }

    var body: some View {
        //Toda estructura tipo View tiene un atributo body donde se encuentran los elementos que queremos poner en la pantalla.
        //Usa el + que hay arriba a la derecha para añadir más cosas a la pantalla.

        Section {
            Label("SwiftUI iLDTS DBApp-1", systemImage: "iphone")
                .font(/*@START_MENU_TOKEN@*/.headline/*@END_MENU_TOKEN@*/)
            Label("by LDTS", systemImage: "signature")
                .font(.subheadline).padding()
        }
        
        
        Divider().padding(.top, 10.0).padding(.bottom, 20.0)
        
        //Gracias a que tapnumber es una variable @State este texto se actualizará cuando cambie su valor
        Text(queryresult)
            .foregroundColor(Color.yellow)
            .padding()
        
        Divider().frame(height: 10.0).foregroundColor(.white).padding(10)

        Button("Execute SQL Query") {
            
            let db = SQLif()
            db.open()
            let query = "SELECT * FROM usuarios ORDER BY random()"
            
            let table = db.query2tabla(queryStatementString: query)

            queryresult = "____________________________ \n"
            for row in table {
                queryresult += "\n"
                for field in row {
                    queryresult +=
                        "  \(field)  "
                }
            }
            
            
        }.padding()
        .frame(width: 250.0)
        .foregroundColor(.black)
        .background(Color(red: 252/255, green: 1, blue: 252/255, opacity: 1.0))
        .clipShape(Rectangle())
        .border(Color.white, width: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/)
        .cornerRadius(/*@START_MENU_TOKEN@*/3.0/*@END_MENU_TOKEN@*/)
    }}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
