//
//  ildts1App.swift
//  ildts1
//
//  Created by Josemaria Carazo Abolafia on 11/5/21.
//

import SwiftUI

@main
struct ildts1App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
