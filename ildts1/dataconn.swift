//
//  dataconn.swift
//  iLDTS-3
//
//  Created by Josemaria Carazo Abolafia on 27/09/2019.
//  Copyright © 2019 Josemaria Carazo Abolafia. All rights reserved.
//

import Foundation
import os.log
import SQLite3

class dataconn {
    
    let dbURL: URL
    // The database pointer.
    var db: OpaquePointer?
    // Prepared statement https://www.sqlite.org/c3ref/stmt.html to insert an event into Table.
    // we use prepared statements for efficiency and safe guard against sql injection.
    var insertEntryStmt: OpaquePointer?
    var readEntryStmt: OpaquePointer?
    var updateEntryStmt: OpaquePointer?
    var deleteEntryStmt: OpaquePointer?
    
    let oslog = OSLog(subsystem: "ildts3", category: "sqlif")
    
    enum SQLiteError: Error {
        case OpenDatabase(message: String)
        case Prepare(message: String)
        case Step(message: String)
        case Bind(message: String)
    }
    
    init() {
        dbURL = (Bundle.main.resourceURL?.appendingPathComponent("data.db"))!
    }
    
    // Command: sqlite3_open(dbURL.path, &db)
    // Open the DB at the given path. If file does not exists, it will create one for you
    func openDB() throws {
        if sqlite3_open(dbURL.path, &db) != SQLITE_OK { // error mostly because of corrupt database
            os_log("error opening database at %s", log: oslog, type: .error, dbURL.absoluteString)
            //            deleteDB(dbURL: dbURL)
            throw SqliteError(message: "error opening database \(dbURL.absoluteString)")
        }
    }
    
    func closeDB() {
        sqlite3_close(db)
    }

    func deleteDB(dbURL: URL) {
        os_log("removing db", log: oslog)
        do {
            try FileManager.default.removeItem(at: dbURL)
        } catch {
            os_log("exception while removing db %s", log: oslog, error.localizedDescription)
        }
    }
    
    func createTables() throws {
        // create the tables if they dont exist.
        
        // create the table to store the entries.
        // ID | Name | Employee Id | Designation
        let ret =  sqlite3_exec(db, "CREATE TABLE IF NOT EXISTS Records (id INTEGER UNIQUE PRIMARY KEY AUTOINCREMENT, Name TEXT NOT NULL, EmployeeID TEXT UNIQUE NOT NULL, Designation TEXT NOT NULL)", nil, nil, nil)
        if (ret != SQLITE_OK) { // corrupt database.
            logDbErr("Error creating db table - Records")
            throw SqliteError(message: "unable to create table Records")
        }
        
    }

    func queryA26(queryStatementString: NSString) -> [[String]] {
        var queryStatement: OpaquePointer? = nil
        var resultado : [[String]]
        resultado = [[""]]
        resultado.removeAll()
        
        if sqlite3_prepare_v2(db, queryStatementString.utf8String, -1, &queryStatement, nil) == SQLITE_OK {
                
            while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                var i = 0
                var fila : [String]
                fila = [""]
                fila.removeAll()
                while (i<sqlite3_column_count(queryStatement)) {
                    let col0 = sqlite3_column_text(queryStatement, Int32(i))
                    if (col0 != nil) { fila.append(String(cString: col0!)) }
                    else { fila.append("") }
                    i=i+1
                }
                resultado.append(fila)
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        
        if (resultado.endIndex<1) {
            resultado.append([""])
        }

        sqlite3_finalize(queryStatement)
        
        return resultado
        
    }
    
    
    func insert(insertStatementString: String) {
        var insertStatement: OpaquePointer? = nil
        
        // 1
        if sqlite3_prepare(db, insertStatementString, -1, &insertStatement, nil) == SQLITE_OK {
            let id: Int32 = 1
            let name: NSString = "Ray"
            
            // 2
            sqlite3_bind_int(insertStatement, 1, id)
            // 3
            sqlite3_bind_text(insertStatement, 2, name.utf8String, -1, nil)
            
            // 4
            if sqlite3_step(insertStatement) == SQLITE_DONE {
                print("Successfully inserted row.")
            } else {
                print("Could not insert row.")
            }
        } else {
            print("INSERT statement could not be prepared.")
        }
        // 5
        sqlite3_finalize(insertStatement)
    }
    
    func update(updateStatementString: String) {
        var updateStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, updateStatementString, -1, &updateStatement, nil) == SQLITE_OK {
            if sqlite3_step(updateStatement) == SQLITE_DONE {
                print("Successfully updated row.")
            } else {
                print("Could not update row.")
            }
        } else {
            print("UPDATE statement could not be prepared")
        }
        sqlite3_finalize(updateStatement)
    }
    
    func delete(deleteStatementStirng: String) {
        var deleteStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, deleteStatementStirng, -1, &deleteStatement, nil) == SQLITE_OK {
            if sqlite3_step(deleteStatement) == SQLITE_DONE {
                print("Successfully deleted row.")
            } else {
                print("Could not delete row.")
            }
        } else {
            print("DELETE statement could not be prepared")
        }
        
        sqlite3_finalize(deleteStatement)
    }
    
    func logDbErr(_ msg: String) {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        os_log("ERROR %s : %s", log: oslog, type: .error, msg, errmsg)
    }
}

// Indicates an exception during a SQLite Operation.
class SqliteError : Error {
    var message = ""
    var error = SQLITE_ERROR
    init(message: String = "") {
        self.message = message
    }
    init(error: Int32) {
        self.error = error
    }
    
}
